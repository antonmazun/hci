var App = App || {};

App.ajax = function () {

    var init = function () {
        send_ajax();
        deleteing_basket();
    };

    var send_ajax = function () {
        $('.js-add-backet').click(function (e) {
            e.preventDefault();
            var id_product = $(this).data('id');
            var category = $(this).data('category');
            console.log('url', $(this).prop('href'));
            // console.log(category);
            $.ajax({
                method: "GET",
                url: $(this).prop('href'),
                data: {
                    id: $(this).data('id'),
                    category: $(this).data('category')
                },
                success: function (data) {
                    console.log(data);
                    $('.js-count').html(data.len)
                    alert('Вы добавили элемент в корзину!');
                    // console.log(data.len);
                }
            });

            console.log($(this).prop('href'));
        });

    };

    var deleteing_basket = function () {
        $('.js-deleting').click(function (e) {
            e.preventDefault();
            if (confirm('Вы точно хотите сделать ?')) {
                $.ajax({
                    url: $(this).attr('href')
                }).done(function (data) {
                    alert(data.ok);
                    window.location.reload();
                    // console.log(data);
                    // $('.ajax-wrapper').html(data.ok);
                    // window.location.reload();
                    // console.log(data.ok);
                })
            } else {
                return false;
            }


        });
        console.log('js-deleting');
    };


    return {
        init: init
    }
}();