from django.shortcuts import render
from django.http import HttpResponse , JsonResponse
from .models import Laptop, Category, Phone, SourceDetail, Basket
from django.views.generic import DetailView

# from baсket.models import Basket
# from baсket.models import Backet


all_categories = Category.objects.all()
all_Baсket_len = len(Basket.objects.all())
# all_Baсket_len = None
category = None


def test(request):
    all_Baсket_len = len(Basket.objects.all())
    laptop_instance = Laptop()
    all_laptop = laptop_instance.get_all()
    # laptop_instance.delete_all()
    ctx = {}
    ctx['category_name'] = 'all'
    ctx['all_laptop'] = all_laptop
    ctx['all_Baсket_len'] = all_Baсket_len
    print(all_laptop)
    return render(request, 'category/index.html', ctx)


def laptops_view(request):
    ctx = {}
    all_laptops = Laptop.objects.all()
    all_categories = Category.objects.all()
    all_Baсket_len = len(Basket.objects.all())

    ctx['objects'] = all_laptops
    ctx['category_name'] = 'laptops'
    ctx['all_categories'] = all_categories
    ctx['title'] = 'Ноутбуки'
    ctx['all_Baсket_len'] = all_Baсket_len

    print(all_categories)
    for elem in all_categories:
        print(elem)
    # print(all_laptops)
    return render(request, 'category/laptops.html', ctx)


def phones_view(request):
    ctx = {}
    all_phones = Phone.objects.all()
    all_Baсket_len = len(Basket.objects.all())
    ctx['objects'] = all_phones
    ctx['category_name'] = 'mobile_phones'
    ctx['title'] = 'Телефони'
    ctx['all_Baсket_len'] = all_Baсket_len
    return render(request, 'category/phones.html', ctx)


class LaptopDetailView(DetailView):
    queryset = Laptop.objects.all()
    template_name = 'category/laptop_detail.html'


    def get_context_data(self, **kwargs):
        all_Baсket_len = len(Basket.objects.all())
        context = super(LaptopDetailView, self).get_context_data(**kwargs)
        try:
            source_detail = SourceDetail.objects.filter(category=Category.objects.get(name='laptops'))
            context['source_detail'] = source_detail
            print(source_detail)
        except:
            pass

        context['category_name'] = 'laptops'
        context['all_categories'] = all_categories
        context['all_Baсket_len'] = all_Baсket_len

        return context

    def get_object(self, queryset=None):
        obj = super().get_object()
        obj.save()
        return obj


class PhoneDetailView(DetailView):
    queryset = Phone.objects.all()
    template_name = 'category/laptop_detail.html'

    def get_context_data(self, **kwargs):
        all_Baсket_len = len(Basket.objects.all())
        context = super(PhoneDetailView, self).get_context_data(**kwargs)
        try:
            source_detail = SourceDetail.objects.filter(category=Category.objects.get(name='mobile_phones'))
            print(source_detail)
            context['source_detail'] = source_detail
        except:
            pass
        context['category_name'] = 'mobile_phones'
        context['all_categories'] = all_categories
        context['all_Baсket_len'] = all_Baсket_len
        return context

    def get_object(self, queryset=None):
        obj = super().get_object()
        obj.save()
        return obj


MODEL_CHOICE = {
    'laptops': Laptop,
}


def search(request):
    ctx = {}
    category = Category.objects.get(name=request.GET.get('category')).name
    all_Baсket_len = len(Basket.objects.all())
    if request.GET.get('to') == '' or request.GET.get('from') == '':
        all_search = MODEL_CHOICE[category].objects.all()
    else:
        all_search = MODEL_CHOICE[category].objects.all().filter(price__lt=request.GET.get('to'),
                                                                 price__gt=request.GET.get('from'))
    ctx['req_category'] = request.GET.get('category')
    ctx['req_price_to'] = request.GET.get('to')
    ctx['req_price_from'] = request.GET.get('from')
    ctx['all_search'] = all_search
    ctx['all_categories'] = all_categories
    ctx['category_name'] = category
    ctx['all_Baсket_len'] = all_Baсket_len
    return render(request, 'category/search.html', ctx)


def source_detail_item(request, id):
    all_Baсket_len = len(Basket.objects.all())
    object_source = SourceDetail.objects.get(id=id)
    ctx = {}
    ctx['object'] = object_source
    ctx['category_name'] = 'source_details'
    print(id)
    return render(request, 'category/source_detail_item.html', ctx)


def add_to_backet(request, id, category):
    target_category = Category.objects.get(name=category)
    Basket.objects.create(name_category=target_category , product_id=id)
    all_Baсket_len = len(Basket.objects.all())
    return JsonResponse({'len': all_Baсket_len
    })



def basket_list(request):
    laptops_category = Category.objects.get(name='laptops')
    mobiles_category = Category.objects.get(name='mobile_phones')
    ctx = {}
    laptops_in_basket  = Basket.objects.filter(name_category=laptops_category)
    list_laptops  = []
    for elem in laptops_in_basket:
        list_laptops.append(Laptop.objects.get(pk=elem.product_id))

    phones_in_basket = Basket.objects.filter(name_category=mobiles_category)
    list_phones = []
    for elem in phones_in_basket:
        list_phones.append(Phone.objects.get(pk=elem.product_id))

    all_Baсket_len = len(Basket.objects.all())
    ctx['all_Baсket_len'] = all_Baсket_len
    ctx['list_laptops'] = list_laptops
    ctx['list_phones'] = list_phones
    return render(request , 'category/basket_list.html' , ctx)



def delete_from_basket(request ,  id, category):
    ctx ={}
    target_category = Category.objects.get(name=category)
    Basket.objects.filter(name_category=target_category , product_id=id)[0].delete()
    ctx['ok'] = 'Success!'
    return JsonResponse({
        'ok' : 'Мы удалили с вашей корзины товар '
    })