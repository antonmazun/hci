# Generated by Django 2.1.3 on 2018-12-12 14:31

import ckeditor_uploader.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('category', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Basket',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product_id', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(choices=[('laptops', 'Ноутбуки'), ('mobile_phones', 'Телефоны'), ('tablet', 'Планшеты'), ('source_details', 'Комплектующие'), ('nau', 'Наушники')], max_length=100, verbose_name='Категория')),
            ],
        ),
        migrations.CreateModel(
            name='Phone',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('model', models.CharField(max_length=255, verbose_name='Модель телефона')),
                ('brand', models.CharField(default='Apple', max_length=255, verbose_name='Бренд ')),
                ('oper_system', models.CharField(choices=[('mac', 'MacOS'), ('linux', 'Linux'), ('windows', 'Windows')], max_length=255, verbose_name='Операционная система')),
                ('oper_memory', models.IntegerField(default=16, verbose_name='ОЗУ')),
                ('count_core', models.IntegerField(default=8, verbose_name='Количество ядер')),
                ('price', models.IntegerField(default='2000', verbose_name='Цена')),
                ('image', models.ImageField(blank=True, null=True, upload_to='images/', verbose_name='Картинка')),
                ('desc', models.TextField(default='no desc', max_length=500, verbose_name='Описание ')),
                ('category', models.ForeignKey(default=2, on_delete=django.db.models.deletion.CASCADE, to='category.Category')),
            ],
        ),
        migrations.CreateModel(
            name='SourceDetail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(default='', max_length=255, verbose_name='Назва')),
                ('short_desc', models.CharField(default='Краткое описание', max_length=255, verbose_name='Краткое описание')),
                ('image', models.ImageField(upload_to='images/')),
                ('price', models.FloatField()),
                ('content', ckeditor_uploader.fields.RichTextUploadingField(default='')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='category.Category', verbose_name='Комлектующие в категории')),
            ],
        ),
        migrations.AddField(
            model_name='laptop',
            name='desc',
            field=models.TextField(default='no desc', max_length=500, verbose_name='Описание '),
        ),
        migrations.AddField(
            model_name='laptop',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='images/', verbose_name='Картинка'),
        ),
        migrations.AlterField(
            model_name='laptop',
            name='brand',
            field=models.CharField(default='Apple', max_length=255, verbose_name='Бренд '),
        ),
        migrations.AlterField(
            model_name='laptop',
            name='count_core',
            field=models.IntegerField(default=8, verbose_name='Количество ядер'),
        ),
        migrations.AlterField(
            model_name='laptop',
            name='diagonal',
            field=models.CharField(default='12', max_length=255, verbose_name='Диагональ '),
        ),
        migrations.AlterField(
            model_name='laptop',
            name='model',
            field=models.CharField(default='i534', max_length=255, verbose_name='Модель ноутбука'),
        ),
        migrations.AlterField(
            model_name='laptop',
            name='oper_memory',
            field=models.IntegerField(default=16, verbose_name='ОЗУ'),
        ),
        migrations.AlterField(
            model_name='laptop',
            name='price',
            field=models.IntegerField(default='50000', verbose_name='Цена'),
        ),
        migrations.AddField(
            model_name='basket',
            name='name_category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='category.Category'),
        ),
        migrations.AddField(
            model_name='laptop',
            name='category',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='category.Category'),
        ),
    ]
